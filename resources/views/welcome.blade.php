<!DOCTYPE html>

<html>
    <head>
        <title>Laravel</title>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="http://cdn.jsdelivr.net/vue/1.0.14/vue.min.js" type="text/javascript"></script>

    </head>
    <body>

        <div id="container">
            <div class="container">
              <div class="panel-group">
                <h1>Form</h1>
                <div class="panel panel-default">
                  <div class="panel-body">
                          <form class="form-horizontal" role="form">



                            <div class="form-group">
                              <label class="control-label col-sm-2" >Project:</label>
                              <div class="col-sm-10">
                                <p class='form-control-static'  >
                                  <select v-model="project_number" v-on:change="projectChange">
                                    <option v-for="option in projects" v-bind:value="option.value">
                                        @{{ option.text }}
                                      </option>
                                  </select>
                                </p>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-sm-2" >Task Name:</label>
                              <div class="col-sm-10">
                                <input type="task_name" v-model='task.name' class="form-control" placeholder="Enter email">
                              </div>
                            </div>

                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <div class='button-group'>
                                  <button type="button"  v-on:click="submit" class="btn btn-default">Submit</button>
                                </div>
                              </div>
                            </div>
                            </form>
                  </div>
                </div>
                <h1>Tasks list</h1>
                <div class="panel panel-default">
                  <div id='inventor' class="panel-body">
                    <table  v-on:mouseover="dragAndDrop" class='table'>
                      <tbody>
                      <tr>
                        <th>ID</th>
                        <th>Priority</th>
                        <th>Name</th>
                        <th>Project Number</th>
                        <th>Datatime</th>
                        <th></th>
                        <th></th>
                      </tr>
                        <tr v-for="ts in tasks" class="ui-draggable ui-draggable-handle">
                          <td>@{{ts.id}}</td>
                          <td>@{{ts.priority}}</td>
                          <td>@{{ts.name}}</td>
                          <td>@{{ts.project_number}}</td>
                          <td>@{{ts.created_at}}</td>
                          <td><button type="button"  v-on:click="edit(ts)" class="btn btn-default">Edit</button></td>
                          <td><button type="button"  v-on:click="delete(ts)" class="btn btn-default">Delete</button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>

            </div>




        </div>

        <script>

var c={}
var dragAndDropInitialized = false
var app = new Vue({
  el: '#container',
  data: {
    tasks : [],
    task : null,
    project_number : 1,
    projects: [
      { text: 'Project A', value: 1 },
      { text: 'Project B', value: 2 },
      { text: 'Project C', value: 3 }
    ]
  },

  ready: function () {
    this.task = this.newTask()
    this.load()

  },

  methods: {


    dragAndDrop : function() {
      //this will hold reference to the tr we have dragged and its helper
      self = this
      if(!dragAndDropInitialized && $('#inventor tr').length>1) {

        dragAndDropInitialized = true
        $("#inventor tr").draggable({
           helper: "clone"
         })

         $("#inventor tr").droppable({
           drop: function(event, ui) {
                var inventor = ui.draggable.text();
                var movedPriority = ui.draggable[0].cells[1].textContent
                var targetPriority = $(this)[0].cells[1].textContent

                var movedTask = self.findTaskByPriority(movedPriority)
                var targetTask = self.findTaskByPriority(targetPriority)
                movedTask.priority = targetPriority
                targetTask.priority = movedPriority

                self.sortTasks()
                self.updateTasks()

                $(c.tr).remove();
                $(c.helper).remove();

            }
          })
     }

    },

    newTask : function() {
      return {
        id : null,
        name : '',
        priority : 0,
        project_number : this.project_number,
        created_at : null
      }
    },

    projectChange : function() {
      this.task.project_number = this.project_number
      this.load()

    },

    edit : function(task) {
      this.task = task
    },

    delete : function(task) {
      this.tasks.splice(this.tasks.indexOf(task),1)
      var max = this.getMaxTaskPriority()
      $.each(this.tasks, function(i,t){
        t.priority = max-1 - i
      })
      this.remove(task)
    },

    remove : function(task) {
      self = this
      $.post( "{{url('tasks/remove')}}", { "task":task, "_token": "{{csrf_token()}}"})
            .done(function( data ) {
              self.updateTasks()
            })
            .fail(function(xhr, err) {
                console.log(xhr)
                console.log(err)
            })
    },

    load : function() {
      self = this
      $.post( "{{url('tasks/load')}}", { "project_number":self.project_number, "_token": "{{csrf_token()}}"})
            .done(function( data ) {
              console.log("Result POST: ", data)
              self.tasks = data
              self.sortTasks()
              dragAndDropInitialized = false
            })
            .fail(function(xhr, err) {
                console.log(xhr)
                console.log(err)
            })
    },

    findTaskByPriority(priority) {
      var result = null
      $.each(this.tasks,function(i,t){
        if(t.priority == priority) result=t
      })
      return result
    },

    updateTasks : function() {
      $.post( "{{url('tasks/update')}}", { 'tasks': self.tasks, "_token": "{{csrf_token()}}"})
            .done(function( data ) {
              console.log("Result POST: ", data)

            })
            .fail(function(xhr, err) {
                console.log(xhr)
                console.log(err)
            })
    },

    submit : function() {
      var self = this
      if(this.task.id == null) {
        this.task.priority = this.getMaxTaskPriority() + 1
      }
      this.saveTask()
      dragAndDropInitialized = false
    },

    saveTask : function() {
      var self = this
      $.post( "{{url('tasks/save')}}", { 'task': self.task, "_token": "{{csrf_token()}}"})
            .done(function( data ) {
              console.log("Result POST: ", data)
              if(self.task.id == null) self.tasks.push(self.task)
              self.task.id = data.id
              self.task.created_at = data.created_at
              self.task = self.newTask()
              self.sortTasks()
            })
            .fail(function(xhr, err) {
                console.log(xhr)
                console.log(err)
            })
    },

    getMaxTaskPriority() {
      var max = 0;
      $.each(this.tasks,function(i,t){
        if(t.priority > max) max=t.priority
      })
      return max
    },

    sortTasks() {
      this.tasks = this.tasks.sort(function(a, b) {
        var a = a.priority
        var b = b.priority
        return a>b ? -1 : a<b ? 1 : 0;
      });
    }
  },
})

        </script>
    </body>
</html>
