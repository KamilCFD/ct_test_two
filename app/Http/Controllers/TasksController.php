<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Task;

class TasksController extends Controller
{
    public function postSave() {

      $taskJS = Input::get('task');

      $task = Task::find($taskJS['id']);
      if(!$task) $task = new Task;

      $task->name = $taskJS['name'];
      $task->priority = $taskJS['priority'];
      $task->project_number = $taskJS['project_number'];
      $task->save();

      return [
              'created_at' => $task->created_at->format('Y-m-d H:i:s'),
              'id' => $task->id
            ];
    }

    public function postUpdate() {

      $tasksJS = Input::get('tasks');

      foreach($tasksJS as $taskJS) {
        $task = Task::find($taskJS['id']);
        $task->name = $taskJS['name'];
        $task->priority = $taskJS['priority'];
        $task->project_number = $taskJS['project_number'];
        $task->save();
      }

      return 'ok';
    }

    private function getMaxId($products) {
      $maxid=0;
      foreach($products as $id => $pr) {
        if($id>$maxid) $maxid=$id;
      }
      return $maxid;
    }

    public function postLoad() {
      $tasks = Task::where('project_number','=',Input::get('project_number'))->get();
      return $tasks;
    }

    public function postRemove() {
      $taskJS = Input::get('task');
      $task = Task::find($taskJS['id']);
      if($task) $task->delete();
      return 0;
    }


}
